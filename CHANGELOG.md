# Changelog
**Attention:** ⚠️ means that a change breaks things. Manual adjustments will be necessary. So be careful before updating. Even data loss might occur.

## Version 1.10.6 (25th of May 2023)
  - Bugfix Popover closing error
  - EditableList dragging fix
  
## Version 1.10.5 (17th of May 2023)
  - input config node event fix
  - input config node filter function wrapper to enable changing node values 

## Version 1.10.4 (someday)
  - just updated dependencies as some old svelte version created invalid pages...

## Version 1.10.3 (30th of March 2023)
  - typedInput blur event fix + focus after select left side option

## Version 1.10.2 (23rd of March 2023)
  - Added missing MenuEntry title for not expandable
  - Popover opening/closing fix
  - Popover added property: closeOtherPopovers
  - added MenuEntry example

## Version 1.10.1 (22nd of March 2023)
  - changed event for update nodes to node runtime-state === start
  - TabbedPane event fix

## Version 1.10.0 (20th of March 2023)
  - code refactoring build-html
  - Added on:click / on:keydown to TabbedPane
  - parse version fix

## Version 1.9.5 (07th of March 2023)
  - Bugfix Input fields checkbox and color events

## Version 1.9.4 (28th of February 2023)
  - Added keydown event and replaced depreceated keypressed if function was called for the component

## Version 1.9.3 (28th of February 2023)
  - Added on focus / blur event to input fields

## Version 1.9.2 (23rd of February 2023)
  - Refactored tooltip
  - Label bugfix typedInput (was empty if created dynamically)

## Version 1.9.1 (23rd of February 2023)
  - Added tooltip to Collapsible, EditableList, Group
  - Added Id for ComponentHeader labels
  - css fix for links in tooltip 
  - refactoring typedInput

## Version 1.9.0 (23rd of February 2023)
  - focus fix TypedInput
  - ⚠️ TypedInput id naming corrected
  - Using Node-Red tooltip (dynamically)
  - Textarea added tooltip

## Version 1.8.10 (20th of February 2023)
  - closing Popover handling fix
  - prevent Popover click propagation

## Version 1.8.9 (15th of February 2023)
  - changed MenuEntry fading
  - validate will have node as second property for more complex validations (default.prop.validate doesn't have "this" on input field).

## Version 1.8.8 (14th of February 2023)
  - added tooltip to TypedInput
  - added tooltip in Readme for Input and TypedInput
  - no-drop cursor and no focus when TypedInput is disabled

## Version 1.8.7 (08th of February 2023)
  - TypedInput Error and no node fix

## Version 1.8.6 (06th of February 2023)
  - TypedInput noValue Options fix
  - Sidebar config nodes counter fix  

## Version 1.8.5 (06th of February 2023)
  - maximize + placeholder for typedInput

## Version 1.8.4 (06th of February 2023)
  - Bugfix TypedInput
  - Set focus on typedInput

## Version 1.8.3 (06th of February 2023)
  - remove temp file fix

## Version 1.8.2 (03rd of February 2023) 
  - Bugfix TypedInput

## Version 1.8.1 (03rd of February 2023)
  - Bugfix TypedInput

## Version 1.8.0 (03rd of February 2023)
- remove timestamp from rendered html file to prevent unnecessary changes / commits
- ⚠️ TypedInput fields were rebuild without jQuery. Default stuff is working, some specializations are currently missing (type node, multiselect, etc.).

## Version 1.7.4 (17th of January 2023)
- depreceated event.path replaced with event.composedPath()

## Version 1.7.3 (09th of January 2023)
- Experimental: Safari seems to have problems with "function update (node) {}" -> changed to "const update = function (node) {}" for user testing!

## Version 1.7.2 (End of Year 2022)
- Code refactoring
- Added test case for TypedInput and evaluateNodeProperty

## Version 1.7.1 (13th of December 2022)
- Popover opening fix
- stop propagation in button readded (got somehow lost...)

## Version 1.7.0 (24th of November 2022)
- ToggleGroup simpler flex direction management
- Popover custom on:click event and tooltip added
- Input field added type "search" for simple Node-RED like search fields

## Version 1.6.0 (15th of November 2022)
- Added ButtonGroup component
- Added tooltip to Button component
- Icon only for Input component
- Popover sets custom clazz in button and popover
- Added ToggleGroup small and showHeader property
- ⚠️changed update property from nodeVersion to _version

## Version 1.5.3 (28th of October 2022)
- Bugfix update node

## Version 1.5.2 (28th of October 2022)
- Button click stop propagation added
- more flexible Component Header (for EditableList, Collapsible or TreeNode) 
- Table set Popup buttons into svelte:fragment 
- test-node set slots into svelte:fragment

## Version 1.5.1 (18th of October 2022)
- Bugfix escaped TypedInput

## Version 1.5.0 (18th of October 2022)
- Escaped TypedInput id to prevent jquery errors
- Added on:keypress where on:click events are (A11y standard)

## Version 1.4.4 (17th of October 2022)
- Bugfix update node

## Version 1.4.3 (22nd of September 2022)
- TabbedPane returns readable error if no or no valid tabs are set
- Console logs start with '\[sir\]'
- PlainInput color click event reacts on the icon instead of the input field

## Version 1.4.2 (08th of September 2022)
- TypedInput waiting for DOM if element was not created yet
- TypedInput reactive placeholder

## Version 1.4.1 (30th of August 2022)
- Added markdown documentation

## Version 1.4.0 (30th of August 2022)
 - ⚠️ Sortable EditableList dispatches now a 'sort' instead of a 'changed' event
 - ⚠️ EditableList remove event returns the removed object in e.detail.value and the index in e.detail.index
 - EditableList TableHeader width fix

## Version 1.3.5 (29th of August 2022)
 - Input Property was wrongly overwritten with PlainInput
 - Css fixes

## Version 1.3.4 (19th of July 2022)
- Popover Added Classes
- Popover out of window on right side bugfix
- Input added on:input event

## Version 1.3.3 (19th of July 2022)
- Popover Bugfix z-style

## Version 1.3.2 (04th of July 2022)
- Placeholder TypedInput can be put in the defaults (like normal input)

## Version 1.3.1 (24th of June 2022)
- fixed missing theme compatible color options

## Version 1.3.0 (20th of June 2022)
- SIR is now theme compatible

## Version 1.2.3 (3rd of June 2022)
- Added Editable List tableHeader

## Version 1.2.2 (2nd of June 2022)
- Added Callout small option
- Callout css fixes

## Version 1.2.1 (23rd of May 2022)
- Bugfix credentials input fields

## Version 1.2.0 (19th of May 2022)
- Updated svelte to 3.48.0 and changed dispatcher call

## Version 1.1.12 (19th of May 2022)
- TypedInput Ace Editor Bugfix

## Version 1.1.11 (28th of April 2022)
- Editable list drag and drop improved
- TreeNode draggable option
- TabbedPane undraggable

## Version 1.1.10 (8th of April 2022)
- Bugfix tooltip

## Version 1.1.9 (8th of April 2022)
- Added tooltip to input fields

## Version 1.1.8 (1st of April 2022)
- Added keypress event to input fields

## Version 1.1.7 (10th of March 2022)
- Input fields can accept falsy values (Checkbox = false, Number = 0)

## Version 1.1.6 (10th of March 2022)
- Editable List: Add Button label naming
- Error handling for Input, Select, Textarea
- Label before Input Checkbox
- Fix calculated width TabbedPane

## Version 1.1.5 (08th of March 2022)
- Bugfix init component

## Version 1.1.4 (03rd of March 2022)
- Experimental: TreeNode only click on icon feature
- Panel: added bottom size calculation
- Small css fixes

## Version 1.1.2 (24th of February 2022)
- Editable list drop fix

## Version 1.1.1 (24th of February 2022)
- Manuell error property for input fields added

## Version 1.1.0 (23rd of February 2022)
- Refactoring change event
- ⚠️Table now uses events instead of functions set in property

## Version 1.0.21 (22nd of February 2022)
- Bugfix Change Event (Select)

## Version 1.0.20 (22nd of February 2022)
- Bugfix Change Event (PlainInput)

## Version 1.0.19 (21st of February 2022)
- Collapsible setting collapsed bugfix
- Components now only trigger change event if their value was changed (instead of node)
- Input Fields now can change the value if node[prop] was changed

## Version 1.0.18 (31st of January 2022)
- Removed console.log from MenuEntry

## Version 1.0.17 (31st of January 2022)
- Renaming TreeElement to TreeNode

## Version 1.0.16 (31st of January 2022)
- Bugfix TreeElement

## Version 1.0.15 (09th of December 2021)

- TabContent added click event
- MenuEntry added max-height

## Version 1.0.14 (02nd of December 2021)

- Collapsible click event and css fix

## Version 1.0.13 (23rd of November 2021)
 - Added recreate TreeElement depth trigger
 - css fixes
 - user-select disabled on all rows (exception: Callout & Table)
 

## Version 1.0.12 (23rd of November 2021)

- Collapsible css fix to prevent wrong collapsible event height
- Fixed indented for EditableList and Collapsible
- Full width header is now an intern component
- Added Panel component
- Added custom component example
- Some other css fixes
- onClick event for all components
- Added TreeElement component

## Version 1.0.11 (05th of November 2021)

- ToggleGroup structure changed to be similar to other components
- Added header slot to Collapsible, EditableList, Group, ToggleGroup
- Label cursor and no more user-selection fix

## Version 1.0.10 (03rd of November 2021)

- Added clazz and style property to all components (except Popup)
- Added click, dblclick, mouseenter and mouseleave event for rows
- Changed left/right scroll icon for TabbedPane

## Version 1.0.9 (27th of October 2021)

- Changed sir-Form-row to sir-Row

## Version 1.0.8 (27th of October 2021)

- Changed sir-Form-row to sir-Row
- EditableList css fix

## Version 1.0.7 (27th of October 2021)

- Set named id for each component container (instead of Row-{id})
- Css class name convention (sir-"Name")
- Popover + MenuEntry Css + transition fix

## Version 1.0.6 (26th of October 2021)

- TabbedPane code changed to svelte without jQuery
- Tabs can now have icons
- TabbedPane has now ScrollButtons instead of Scrollbar

## Version 1.0.5 (22th of October 2021)

- Code cleaning and added icon to Popover button
- Popup uses now id instead of classes
- EditableList is now using svelte instead of jQuery for sorting
- EditableList has now minHeight and maxHeight property
- MenuEntry css changed

## Version 1.0.3 (18th of October 2021)

- Added TypedInput placeholder
- some missing component ID fixes
- Experimental: Added Popover and MenuEntry Components

## Version 1.0.2 (11th of October 2021)

- Added input color picker

## Version 1.0.1 (8th of October 2021)

- Editable List fix committed :)

## Version 1.0.0 (8th of October 2021)

- EditableList id property can be set manually
- Editable List fixed sorting
- Bugfix Sidebar detection in build-html
- Row transition fade out deactivated
- Table transition fixed
- TabbedPane css fix
- TypedInput Options left border added

## Version 0.1.91 (5th of October 2021)

- EditableList css fix and fading added
- TabbedPane Scrollbar css fix

## Version 0.1.90 (4th of October 2021)

- Callout indented property added
- TypedInput display error fix when disabled + typo fix
- Added custom scrollbar for TabbedPane component
- jumping components better fix implemented

## Version 0.1.89 (23rd of September 2021)

- jumping components fixed if they are switched and fading was active
- flickering when adding a new tab fixed

## Version 0.1.88 (22nd of September 2021)

- fixed typo
- Callout css fixes
- Table edit css fixes

## Version 0.1.87 (30th of August 2021)

- Code convention: script -> style -> html
- ToggleGroup, Group and EditableList small fixes
- Popup only adds button div if existing

## Version 0.1.86 (27th of August 2021)

- Callout and Group styling fix
- Button inline default is false
- Fading feature for all components. Default set to true.

## Version 0.1.85 (27th of August 2021)

- build html bugfix for sidebar width
- Table input styling fix

## Version 0.1.84 (25th of August 2021)

- build html bugfix if node is a sidebar tab
- ToggleGroup indented fix

## Version 0.1.83 (24th of August 2021)

- render function has now an option to set min-width of the node (render(this, { minWidth: "500px;" }))
- added svelte plugin as extension recommendation + install at start
- Row has indentation
- Callout close butto shifted a bit into the box

## Version 0.1.82 (24th of August 2021)

- Row fading added
- Workaround to fix node width bug: Fixed node width on opening and then changing to auto

## Version 0.1.81 (23rd of August 2021)

- Collapsible styling fix

## Version 0.1.80 (23rd of August 2021)

- Collapsible styling fix
- ToggleGroup styling fix

## Version 0.1.79 (23rd of August 2021)

- Row is now flex instead of inline:flex -> can lead to a big min-width for the node by Node-RED
- Style fix TypedInput
- EditableList fix for recursive calling
- Popup Button fix
- Table fix

## Version 0.1.78 (18th of August 2021)

- General styling fixes
- Code convention: script -> style -> html
- Callout works now with slots

## Version 0.1.77 (18th of August 2021)

- Callout can be closable
- Readme

## Version 0.1.75 (18th of August 2021)

- fixed i18n bug
- typedInput styling fixed
- Readme changed

## Version 0.1.73 (18th of August 2021)

- indented property (instead of indentation)
- styling fixes within editable lists

## Version 0.1.72 (16th of August 2021)

- small styling fixes

## Version 0.1.71 (16th of August 2021)

- small styling fixes
- button indented

## Version 0.1.70 (16th of August 2021)

- button + button group styling

## Version 0.1.69 (16th of August 2021)

- popup slot for buttons
- overhaul of css -> using flexboxes instead node-red standard
- added Readme description.

## Version 0.1.68 (04th of August 2021)

- table delete callback returns old row

## Version 0.1.67 (04th of August 2021)

- bugfix and small table changes
- Buttons have id

## Version 0.1.66 (03th of August 2021)

- added more table functions

## Version 0.1.65 (29th of July 2021)

- added callback functions for table
- Popup bugfix

## Version 0.1.64 (29th of July 2021)

- small styling fixes
- added focus for Popup

## Version 0.1.63 (28th of July 2021)

- rollup dependency version set fix to 2.36.1 (last known working)

## Version 0.1.62 (28th of July 2021)

- Bugfix

## Version 0.1.61 (28th of July 2021)

- Table component translation changed as using i18n breaks other translations since Node-RED 2.0.3
- Set styles to class

## Version 0.1.60 (27th of July 2021)

- Popups use Node-Red mechanism
- gitpod.yml using latest node-red version
- Collapsible and Row style to class fixing
- Customizable Row

## Version 0.1.59 (23rd of July 2021)

- SIR own Popup mechanism created
- Table has new SIR Popup
- Button added primary class

## Version 0.1.58 (21th of July 2021)

- Added i18n translation support for tabs
- Added i18n translation support for SIR components
- better show Popup mechanism
- Added Table component
- Label styling fix

## Version 0.1.57 (19th of July 2021)

- TypedInput label on:click -> focus on field
- Added i18n translation support
  
## Version 0.1.56 (15th of July 2021)

- Added Popup
- Added Validation for Input / TypedInput while typing
- Bugfix ConfigNode deleting last option crashed node
- Validating ConfigNode on:blur event
- cleaning code

## Version 0.1.55 (13th of July 2021)

- Added Textare
- Added ToggleGroup
- cleaning code

## Version 0.1.54 (9th of July 2021)

- Added Callout component

## Version 0.1.53 (2nd of July 2021)

- Bugfix empty values set the node dirty flag
- Bugfix deleting config nodes could crash node
