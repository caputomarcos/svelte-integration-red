export function closeOptions () {
  // other TypedInput-Options could be open therefor using classes instead of variables like class:hide={optionHidden}
  document.querySelectorAll('.sir-typedInput-options').forEach(o => o.classList.add('hide'))
  document.querySelectorAll('.sir-typedInput-valueOptions').forEach(o => o.classList.add('hide'))
  removeEventListeners()
}

function removeEventListeners () {
  document.removeEventListener('click', closeOptions)
  document.removeEventListener('keydown', optionKeys)
}

export function optionKeys (e) {
  if (e.code === 'Escape') {
    e.preventDefault()
    closeOptions()
  } else if (e.code === 'ArrowDown') {
    e.preventDefault()
    document.activeElement.nextSibling?.focus()
  } else if (e.code === 'ArrowUp') {
    e.preventDefault()
    document.activeElement.previousSibling?.focus()
  }
}

export const getOptionsPosition = (buttonPos, id) => {
  let button, options
  if (buttonPos === 'left') {
    button = document.getElementById('sir-TypedInput-Options-Button-' + id)
    options = document.getElementById('sir-TypedInput-Options-' + id).getBoundingClientRect()
  } else {
    button = document.getElementById('sir-TypedInput-Value-Options-Button-' + id)
    options = document.getElementById('sir-TypedInput-Value-Options-' + id).getBoundingClientRect()
  }
  const buttonDOMRect = button.getBoundingClientRect()
  let top = buttonDOMRect.bottom
  if (top + options.height > window.innerHeight) {
    top = buttonDOMRect.top - options.height
  }
  let left = buttonDOMRect.left
  if (left + options.width > window.innerWidth) {
    left = left - options.width + buttonDOMRect.width
  }
  return 'top: ' + top + 'px; left: ' + left + 'px; min-width: ' + buttonDOMRect.width + 'px;'
}
