/* global RED */

export const getBooleanFrom = (property) => {
  return typeof property === 'boolean' ? property : property.toString().toLowerCase() === 'true'
}

export const getId = (prop = null) => {
  let id = prop
  if (!id) {
    id = Date.now() + Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)
  }
  return id
}

export const getFadeDuration = (fading) => {
  let fadeDuration = 800
  // do not use isNaN with boolean!
  if (typeof fading === 'number' || (typeof fading === 'string' && fading !== 'true' && fading !== 'false')) {
    fadeDuration = Number(fading)
  } else {
    fading = getBooleanFrom(fading)
    if (!fading) fadeDuration = 0
  }
  return fadeDuration
}

const setInternalValue = (internal, nodeProp, value) => {
  // value must not be undefined, as that can result in strange behaviour but can have other falsy value types (checkbox = false, number = 0)
  if (internal.updateNode && typeof nodeProp !== 'undefined') {
    return nodeProp
  } else if (typeof value !== 'undefined') {
    return value
  } else {
    return ''
  }
}

export const initInternal = (nodeProp, value) => {
  const internal = {
    init: true,
    isError: false,
    updateNode: typeof nodeProp !== 'undefined',
    valueHasChanged: false
  }
  // on init set the nodeProp.value else we have the whole node property (with label etc.)
  internal.value = setInternalValue(internal, nodeProp?.value, value)
  internal.oldValue = internal.value
  return internal
}

export const getNewInternal = (nodeProp, value, internal, validateFunction, error, node) => {
  internal.valueHasChanged = internal.updateNode ? nodeProp !== internal.value : internal.value !== value
  if (internal.valueHasChanged) {
    if (internal.value === internal.oldValue) {
      internal.value = setInternalValue(internal, nodeProp, value)
    }
    internal.oldValue = internal.value
  }
  if (validateFunction) {
    // add node to validate function to have access to node properties while validating when input field changes
    internal.isError = !validateFunction(internal.value, node)
  } else {
    internal.isError = error
  }
  return internal
}

// escape "unallowed" jQuery chars
export const idSanitizer = (id) => id.replace(/(:|\.|\[|\]|,|=|@)/g, '\\$1')

export const setTooltip = (tooltip, tooltipExists, target) => {
  target = idSanitizer(target)

  // delete via popover.tooltip as RED.popover Object has no delete function and removing content will show empty tooltip
  RED.popover.tooltip(window.$(target)).delete()
  tooltipExists = false
  if (tooltip) {
    // Use RED.popover.create to set the tooltip instead of RED.popover.tooltip as a tooltip can't be interactive (closes on mouseover)
    // don't use a dynamic tooltip via content: () => {tooltip} as this won't work so well on some elements (e.g. label) and Node-RED won't
    // use the string rendered as html but as plain string.
    RED.popover.create({
      tooltip: true,
      target: window.$(target),
      trigger: 'hover',
      size: 'small',
      direction: 'bottom',
      content: RED.utils.renderMarkdown(RED.utils.sanitize(tooltip)),
      delay: { show: 750, hide: 100 },
      interactive: true,
      class: 'sir-NR-popover'
    })
    tooltipExists = true
  }
  return tooltipExists
}
