Welcome to the documentation of the second test node of SIR.

This time in markdown.

### Inputs (just kidding, this node has no inputs)

: payload (string | buffer) :  the payload of the message to publish.
: *topic* (string)          :  the MQTT topic to publish to.


### Outputs (and no outputs... this is a just a test documentation)

1. Standard output
: payload (string) : the standard output of the command.

2. Standard error
: payload (string) : the standard error of the command.