const fs = require('fs')
const path = require('path')
const rollup = require('rollup')
const svelte = require('rollup-plugin-svelte')
const { nodeResolve } = require('@rollup/plugin-node-resolve')
const commonjs = require('@rollup/plugin-commonjs')

const createHtmlNode = (nodeUpdateCode, componentName, nodeName, version, code, documentationType, documentation) => `
<script type='text/javascript'>
  "use strict"
  {
    ${nodeUpdateCode}
    const sir_render = function (node, options) {
      try {
        if (typeof node !== 'object') {
          return
        }
        let minWidth = '400px'
        if (options) {
          if (options.minWidth) minWidth = options.minWidth
        }
        if (!node.__clone) {
          node.__clone = window.$.extend(true, {}, node)
        }
        new ${componentName}({
          target: document.getElementById('${nodeName}-svelte-container'),
          props: { node: node.__clone }
        })
        document.getElementById('${nodeName}-svelte-container').style.width = minWidth
        const nodeIsSidebarTab = !!node.onchange
        if (!nodeIsSidebarTab) {
          const orgResize = node._def.oneditresize
          node._def.oneditresize = function (size) {
            document.getElementById('${nodeName}-svelte-container').style.width = 'auto'
            if (orgResize) orgResize(size)
            node._def.oneditresize = orgResize
          }
        }
      } catch (e) {
          console.log(e)
      }
    }
    const sir_update = function (node) {
      if (node.__clone) {
        const clone = node.__clone
        delete node.__clone
        clone._version = "${version}"
        const defaultKeys = Object.keys(node._def.defaults)
        for (const key of Object.keys(clone)) {
          if (defaultKeys.indexOf(key) === -1) {
            delete clone[key]
          }
        }
        // update config node sidebar
        let updateConfigSidebar = false
        for (const key of defaultKeys) {
          if (node._def.defaults[key].type && node[key] !== clone[key]) {
            updateConfigSidebar = true
            // config has changed, add to or remove from config node and refresh sidebar
            // The official RED.nodes.updateConfigNodeUsers(configNode) seems to have no effect.
            const oldConfig = RED.nodes.node(node[key])
            const newConfig = RED.nodes.node(clone[key])
            // Remove from old config node
            if (oldConfig) {
              oldConfig.users = oldConfig.users.filter(userNode => userNode.id !== node.id)
            }
            // add to new config node
            if (newConfig && !newConfig.users?.find(userNode => userNode.id === node.id)) {
              newConfig.users.push(RED.nodes.node(node.id))
            }
          }
        }
        if (updateConfigSidebar) {
          RED.sidebar.config.refresh()
        }

        Object.assign(node, clone)
      }
    }
    const sir_revert = function (node) {
      delete node.__clone
    }
    const sir_addCurrentNodeVersion = function (node) {
      node._version = '${version}'
    }
    ${code}
  }
</script>
<script type="text/x-red" data-template-name="${nodeName}">
  <div id='${nodeName + '-svelte-container'}'></div>
</script>
<script type="${documentationType}" data-help-name="${nodeName}">
    ${documentation}
</script>
`
const createUpdateCode = (componentName, nodeName, updateFilepath, version) => {
  let nodeUpdates = ''
  let { clientUpdate } = require(updateFilepath)
  if (clientUpdate) {
    if (clientUpdate.toString().trim().startsWith('function')) {
      clientUpdate = clientUpdate.replace('function', `function ${componentName}Update`)
    } else {
      clientUpdate = `const ${componentName}Update = ` + clientUpdate
    }
    nodeUpdates = `
    function parseVersion (v) {
      if (!v) v = '0.0.0'
      const versionArray = v.split('.')
      const major = versionArray.shift() || 0
      const minor = versionArray.shift() || 0
      let patch = versionArray.join('.') || '0'
      let tag = ''
      if (patch.includes('-')) {
        const patchAndTag = patch.split('-')
        patch = patchAndTag.shift()
        tag = patchAndTag.shift() 
      }
      return {
        major: Number(major),
        minor: Number(minor),
        patch: Number(patch),
        tag,
        string: v
      }
    }
  ${clientUpdate}
  function doUpdateOnStart () {
    let notificationShowed = false
    let isDirty = false
    setTimeout(function () {
      RED.nodes.eachNode(node => {
        if (node.type === '${nodeName}' && node._version !== '${version}') {
          if (!notificationShowed) {
            RED.notify('Updated node ' + node.type + ' - ' + node.name + ' from _version ' + node._version + ' to ${version}. Please check the node or deploy the automatic update changes.')
            console.log('[sir] Updated node ' + node.type + ' - ' + node.name + ' from _version ' + node._version + ' to ${version}. Please check the node or deploy the automatic update changes.')
            notificationShowed = true
          }
          node = ${componentName}Update(parseVersion(node._version), node)
          node._version = '${version}'
          node.dirty = true
          node.changed = true
          isDirty = true
        }
      })
      if (isDirty) {
        RED.nodes.dirty(true)
      }
      RED.view.select("") // update UI
    }, 1000);
  }
  RED.events.on('runtime-state', (msg) => {
    if (msg.state === 'start') {
      doUpdateOnStart()
    }
  })
  `
  }
  return nodeUpdates
}

function build (baseDir) {
  const packageJson = JSON.parse(fs.readFileSync(path.join(baseDir, 'package.json')))
  const nodes = packageJson['node-red'].nodes
  const version = packageJson.version
  const keys = Object.keys(nodes)
  for (const node of keys) {
    const nodeJs = path.join(baseDir, nodes[node])
    const nodeSvelte = nodeJs.replace('.js', '.svelte')
    const nodeName = node
    if (fs.existsSync(nodeSvelte)) {
      console.log(`[sir] Found svelte-file ${nodeSvelte} for node ${node}.`)

      // rewrite common functions to prevent name clashing ("use strict" mode needed for Safari leads to problem with e.g. "update" function for svelte store)
      let code = fs.readFileSync(nodeSvelte, 'utf8')
      code = code.replace(/<script.*context=("|')module("|')>/, `<script context="module">
        const render = sir_render
        const update = sir_update
        const revert = sir_revert
        const addCurrentNodeVersion = sir_addCurrentNodeVersion
      `)
      // rollup virtual seems to have problems with <script /> parts
      // Therefor saving temp file and removing it later.
      const tempFile = nodeSvelte.replace('.svelte', '_' + Date.now() + '.svelte')
      fs.writeFileSync(tempFile, code)

      // Build page
      rollup.rollup({
        input: tempFile,
        plugins: [
          svelte({
            extensions: ['.svelte'],
            emitCss: false
          }),
          nodeResolve({
            browser: true,
            dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            preferBuiltins: false
          }),
          commonjs()
        ],
        inlineDynamicImports: true,
        output: {
          format: 'cjs',
          exports: 'named'
        }
      }).then((result) => {
        result.generate({}).then((bundle) => {
          // Create wrapper html
          const wrapperFile = nodeJs.replace('.js', '.html')
          const filename = nodeJs.replace('.js', '')
          let documentationType = 'text/html'
          let documentation = 'No documentation yet.'
          if (fs.existsSync(filename + '.doc.html')) {
            documentation = fs.readFileSync(filename + '.doc.html')
          } else if (fs.existsSync(filename + '.doc.md')) {
            documentation = fs.readFileSync(filename + '.doc.md')
            documentationType = 'text/markdown'
          }
          let code = bundle.output[0].code
          // const componentName = code.match(/export default (?<component>\S+);/).groups.component
          // Remove the timestamp from the code name to prevent unnecessary file changes / commits
          const componentNameWithTimestamp = code.match(/export { (?<component>\S+) as default };/).groups.component
          const componentNameArray = componentNameWithTimestamp.split('_')
          const timestamp = componentNameArray.pop()
          const componentName = componentNameArray.join('_')
          code = code.replaceAll(componentNameWithTimestamp, componentName)
          code = code.replaceAll('_' + timestamp + '.svelte', '.svelte') // svelte comments
          // It wouldn't work as a module as then the node would get registered too late
          code = code.replace(/export { \S+ as default };/, '')
          let nodeUpdateCode = ''
          const updateNode = nodeJs.replace('.js', '-update.js')
          if (fs.existsSync(updateNode)) {
            const updateNodePath = path.resolve(updateNode)
            nodeUpdateCode = createUpdateCode(componentName, nodeName, updateNodePath, version)
          }
          const htmlNode = createHtmlNode(nodeUpdateCode, componentName, nodeName, version, code, documentationType, documentation)
          fs.writeFileSync(wrapperFile, htmlNode)
          console.log('[sir] Created HTML version of ' + nodeSvelte)
        })
      }).catch((error) => {
        console.log('[sir]', error)
      }).finally(() => {
        // remove temp file
        fs.unlinkSync(tempFile)
      })
    } else {
      console.log(`[sir] No svelte-file found for node ${node}.`)
    }
  }
}

module.exports = build
